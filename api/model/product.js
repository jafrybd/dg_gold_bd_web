const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');



let getProductList = async () => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getProductList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getProductByID = async (id = 1) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getProductByID(), id, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


module.exports = {
    getProductList,
    getProductByID
}
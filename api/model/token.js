const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');


let getTokenBySend_boxAndTokenGenerateReason = async (send_box = "", GenerateReason = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getTokenBySend_boxAndTokenGenerateReason(), [send_box, GenerateReason], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewToken = async (info = {}) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewToken(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getTokenBySend_boxAndtrack_no = async (send_box = "", track_no = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getTokenBySend_boxAndtrack_no(), [send_box, track_no], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getTokenByTrack_no = async ( track_no = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getTokenByTrack_no(), track_no, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deleteTokenByTrack_no = async ( track_no = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.deleteTokenByTrack_no(), track_no, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}





module.exports = {
    getTokenBySend_boxAndTokenGenerateReason,
    addNewToken,
    getTokenBySend_boxAndtrack_no,
    getTokenByTrack_no,
    deleteTokenByTrack_no
}
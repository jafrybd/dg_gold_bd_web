const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');



let getAdminByEmail = async (email = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getAdminByEmail(), [email], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewAdmin = async (info = {}) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewAdmin(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}





module.exports = {
    getAdminByEmail,
    addNewAdmin
}
const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');


let getUserBalanceByUserId = async (user_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserBalanceByUserId(), [user_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewBalance = async (info = {}) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewBalance(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateBalanceById = async (id, data) => {
    // get object, generate an array and push data value here
    let keys = Object.keys(data);
    let dataParametter = [];

    for (let index = 0; index < keys.length; index++) {
        dataParametter.push(data[keys[index]]);
    }

    return new Promise((resolve, reject) => {
        connectionIntracoLPG.query(quaries.updateBalanceById(data), [...dataParametter, id], (error, result, fields) => {
            if (error) reject(error);
            else resolve(result);
        });
    });
}

module.exports = {
    getUserBalanceByUserId,
    addNewBalance,
    updateBalanceById
}
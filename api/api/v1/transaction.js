const express = require("express");
const router = express.Router();

const isEmpty = require("is-empty");
const uuidv4 = require("uuid");
const jwt = require('jsonwebtoken');

const user_model = require("../../model/user");
const balance_model = require("../../model/balance");
const common_model = require("../../model/common");
const gold_type_model = require("../../model/gold_type");
const buy_sell_trangaction_model = require("../../model/buy_sell_trangaction");

const verifyToken = require("../../jwt/verify/api_v1/verifyToken");

router.post('/check_money_to_gold', verifyToken, async (req, res) => {

    let reqData = {
        "karat_type": req.body.karat_type ?? "22",
        "gold_amount": req.body.gold_amount ?? 0,
        "cal_type": req.body.cal_type ?? 1  // 1 = vori,  2 = gram
    }

    let gold_details = await gold_type_model.getGoldTypeByName(reqData.karat_type);

    if (isEmpty(gold_details) || reqData.gold_amount <= 0) {
        return res.status(200).send({
            "status": 400,
            'message': "No Gold Found. Give valid Gold amount.",
            "success": false,
            "next_step": "buy-sell-buy"
        });
    }

    let getGoldPriceAmount = await common_model.goldToTakaConvert(reqData.gold_amount, gold_details[0].vori_price, reqData.cal_type);
    let vat_amount = Math.round(getGoldPriceAmount * (await common_model.getVATPercentis() / 100));

    return res.send({
        "success": true,
        "message": "",
        "karat_type": reqData.karat_type,
        "cal_type": reqData.cal_type,
        "gold_amount": reqData.gold_amount,
        "gold_price": getGoldPriceAmount,
        "VAT": vat_amount,
        "total": Math.round(getGoldPriceAmount + vat_amount),
        "next_step": "buy-sell-buy-2"
    });
});

router.post('/confirm_money_to_gold', verifyToken, async (req, res) => {

    let reqData = {
        "karat_type": req.body.karat_type ?? "22",
        "gold_amount": req.body.gold_amount ?? 0,
        "cal_type": req.body.cal_type ?? 1  // 1 = vori,  2 = gram
    }

    let gold_details = await gold_type_model.getGoldTypeByName(reqData.karat_type);

    if (isEmpty(gold_details) || reqData.gold_amount <= 0) {
        return res.status(200).send({
            "status": 400,
            'message': "No Gold Found. Give valid Gold amount.",
            "success": false,
            "next_step": "buy-sell-buy"
        });
    }

    let getGoldPriceAmount = await common_model.goldToTakaConvert(reqData.gold_amount, gold_details[0].vori_price, reqData.cal_type);
    let vat_amount = Math.round(getGoldPriceAmount * (await common_model.getVATPercentis() / 100));

    let track_no = uuidv4();
    let todayTime = await common_model.getTodayDateTime();


    let temp_buy_sell_transaction = {
        "user_id": req.decoded.user_id,
        "track_no": track_no,
        "from_currency_type": "money",
        "to_currency_type": "gold",
        "gold_type": reqData.karat_type == 22 ? 1 : (reqData.karat_type == 21) ? 2 : 3,
        "gold_per_gram_price": Math.round(gold_details[0].vori_price / await common_model.getCurrentVoryWightInGram()),
        "sub_total_money": getGoldPriceAmount,
        "vat_money": vat_amount,
        "making_cost": 0,
        "total_money": Math.round(getGoldPriceAmount + vat_amount + 0),
        "total_gold": reqData.cal_type == 1 ? reqData.gold_amount * await common_model.getCurrentVoryWightInGram() : reqData.gold_amount,
        "created_at": todayTime
    }

    let trangaction = await buy_sell_trangaction_model.addNewTempBuyAndSaleTransaction(temp_buy_sell_transaction);

    return res.send({
        "success": true,
        "message": "",
        "karat_type": reqData.karat_type,
        "cal_type": reqData.cal_type,
        "gold_amount": reqData.gold_amount,
        "gold_price": getGoldPriceAmount,
        "VAT": vat_amount,
        "total": Math.round(getGoldPriceAmount + vat_amount),
        "track_no": track_no,
        "next_step": "buy-sell-buy-getaway"
    });
});

router.post('/confirm_money_to_gold_track_no', verifyToken, async (req, res) => {

    let reqData = {
        "track_no": req.body.track_no ?? "0"
    }

    let temp_buy_sell_info = await buy_sell_trangaction_model.getUserTempBuyAndSaleTransactionByTrackNo(reqData.track_no);

    if (isEmpty(temp_buy_sell_info)) {
        return res.send({
            "success": false,
            "message": "Unknown Buy Request",
            "next_step": "buy-sell-buy"
        });
    }

    if (temp_buy_sell_info[0].from_currency_type != "money") {
        return res.send({
            "success": false,
            "message": "Wrong request",
            "next_step": "sale-buy-sale"
        });
    }

    if (temp_buy_sell_info[0].status == 0) {
        return res.send({
            "success": false,
            "message": "Already Complete",
            "next_step": "buy-sell-buy"
        });
    }


    let buy_sell_new = { ...temp_buy_sell_info[0] };
    if (buy_sell_new.hasOwnProperty('track_no')) delete buy_sell_new.track_no;
    if (buy_sell_new.hasOwnProperty('status')) delete buy_sell_new.status;
    if (buy_sell_new.hasOwnProperty('id')) delete buy_sell_new.id;

    let user_balance = await balance_model.getUserBalanceByUserId(temp_buy_sell_info[0].user_id);
    let karat_col_name = "gold_18_karat";

    let userOldBalance = 0
    if (isEmpty(user_balance)) {
        await balance_model.addNewBalance({ "user_id": temp_buy_sell_info[0].user_id });
    } else {
        if (temp_buy_sell_info[0].gold_type == 1) {
            userOldBalance = user_balance[0].gold_22_karat;
            karat_col_name = "gold_22_karat";
        } else if (temp_buy_sell_info[0].gold_type == 2) {
            userOldBalance = user_balance[0].gold_21_karat;
            karat_col_name = "gold_21_karat";
        } else if (temp_buy_sell_info[0].gold_type == 3) {
            userOldBalance = user_balance[0].gold_18_karat;
            karat_col_name = "gold_18_karat";
        } else {
            console.log("come");
        }
    }


    let userNewBalance = userOldBalance + temp_buy_sell_info[0].total_gold;
    let result = await buy_sell_trangaction_model.addNewBuy_sellAndUpdateTemp_buy_sellAndUserGoldBalance(temp_buy_sell_info[0].track_no, buy_sell_new, userNewBalance, karat_col_name, temp_buy_sell_info[0].user_id);

    return res.send({
        "success": true,
        "message": "SuccessFully Buy The Gold",
        "next_step": "move-to-balance-page"
    });
});

router.post('/check_gold_to_money', verifyToken, async (req, res) => {

    let user_id = req.decoded.user_id;
    let gold_type = 1;


    let reqData = {
        "karat_type": req.body.karat_type ?? "22",
        "gold_amount": req.body.gold_amount ?? 0,
        "cal_type": req.body.cal_type ?? 1  // 1 = vori,  2 = gram
    }

    let gold_details = await gold_type_model.getGoldTypeByName(reqData.karat_type);

    if (isEmpty(gold_details)) {
        return res.status(200).send({
            "status": 400,
            'message': "No Gold Found. Give valid Gold amount.",
            "success": false,
            "next_step": "sale-buy-sale"
        });
    }

    let gold_req_balance_gram = reqData.cal_type == 1 ? reqData.gold_amount * await common_model.getCurrentVoryWightInGram() : reqData.gold_amount;

    let user_balance = await balance_model.getUserBalanceByUserId(user_id);

    let userOldBalance = 0
    if (isEmpty(user_balance)) {
        await balance_model.addNewBalance({ "user_id": user_id });
    } else {
        if (reqData.karat_type == "22") {
            userOldBalance = user_balance[0].gold_22_karat;
            gold_type = 1;
        } else if (reqData.karat_type == "21") {
            userOldBalance = user_balance[0].gold_21_karat;
            gold_type = 2;
        } else if (reqData.karat_type == "18") {
            userOldBalance = user_balance[0].gold_18_karat;
            gold_type = 3;
        } else {
            console.log("come");
            gold_type = 0;
        }
    }

    let gold_type_details = await gold_type_model.getGoldTypeById(gold_type);

    // console.table(gold_type_details);

    if (gold_req_balance_gram > userOldBalance || gold_req_balance_gram < (await common_model.getGramWidrowLimitInGram()).MIN) {
        return res.send({
            "success": false,
            "message": "Limite Balance",
            "status": 400,
            "next_step": "sale-buy-sale"
        });
    }

    let getGoldPriceAmount = await common_model.goldToTakaConvert(reqData.gold_amount, gold_details[0].vori_price, reqData.cal_type);

    let gold_balance_by_month = await common_model.gold_available_balance_by_month(user_id, gold_type);

    // Generate Service charge
    let service_charge = await common_model.calculate_service_charge(gold_balance_by_month, gold_req_balance_gram, gold_type_details[0].gram_price);

    // console.table(service_charge);
    // console.table(gold_balance_by_month);

    return res.send({
       "success": true,
        "message": "",
        "karat_type": reqData.karat_type,
        "cal_type": reqData.cal_type,
        "gold_amount": reqData.gold_amount,
        "gold_price": getGoldPriceAmount,
        "service_charge": service_charge.total_service_charge,
        "service_charge_details": service_charge,
        "VAT": 0,
        "total": Math.round(getGoldPriceAmount - service_charge.total_service_charge),
        "next_step": "sell-buy-sell2"
    })
})


router.post('/confirm_gold_to_money', verifyToken,   async (req, res) => {

    let user_id = req.decoded.user_id;
    let gold_type = 1;

    let reqData = {
        "karat_type": req.body.karat_type ?? "22",
        "gold_amount": req.body.gold_amount ?? 0,
        "cal_type": req.body.cal_type ?? 1  // 1 = vori,  2 = gram
    }

    let gold_details = await gold_type_model.getGoldTypeByName(reqData.karat_type);

    if (isEmpty(gold_details) || reqData.gold_amount <= 0) {
        return res.status(200).send({
            "status": 400,
            'message': "No Gold Found. Give valid Gold amount.",
            "success": false,
            "next_step": "buy-sell-buy"
        });
    }

    // -- start 
    let gold_req_balance_gram = reqData.cal_type == 1 ? reqData.gold_amount * await common_model.getCurrentVoryWightInGram() : reqData.gold_amount;

    let user_balance = await balance_model.getUserBalanceByUserId(user_id);

    let userOldBalance = 0
    if (isEmpty(user_balance)) {
        await balance_model.addNewBalance({ "user_id": user_id });
    } else {
        if (reqData.karat_type == "22") {
            userOldBalance = user_balance[0].gold_22_karat;
            gold_type = 1;
        } else if (reqData.karat_type == "21") {
            userOldBalance = user_balance[0].gold_21_karat;
            gold_type = 2;
        } else if (reqData.karat_type == "18") {
            userOldBalance = user_balance[0].gold_18_karat;
            gold_type = 3;
        } else {
            console.log("come");
            gold_type = 0;
        }
    }

    let gold_type_details = await gold_type_model.getGoldTypeById(gold_type);

    // console.table(gold_type_details);

    if (gold_req_balance_gram > userOldBalance || gold_req_balance_gram < (await common_model.getGramWidrowLimitInGram()).MIN) {
        return res.send({
            "success": false,
            "message": "Limite Balance",
            "status": 400,
            "next_step": "sale-buy-sale"
        });
    }

    let getGoldPriceAmount = await common_model.goldToTakaConvert(reqData.gold_amount, gold_details[0].vori_price, reqData.cal_type);

    let gold_balance_by_month = await common_model.gold_available_balance_by_month(user_id, gold_type);

    // Generate Service charge
    let service_charge = await common_model.calculate_service_charge(gold_balance_by_month, gold_req_balance_gram, gold_type_details[0].gram_price);

    // ---- end

    let track_no = uuidv4();
    let todayTime = await common_model.getTodayDateTime();


    let temp_buy_sell_transaction = {
        "user_id": user_id,
        "track_no": track_no,
        "from_currency_type": "gold",
        "to_currency_type":  "money",
        "gold_type": gold_type,
        "gold_per_gram_price": Math.round(gold_details[0].vori_price / await common_model.getCurrentVoryWightInGram()),
        "sub_total_money": getGoldPriceAmount,
        "vat_money": 0,
        "making_cost": 0,
        "service_charge": service_charge.total_service_charge,
        "total_money": Math.round(getGoldPriceAmount - service_charge.total_service_charge),
        "total_gold": reqData.cal_type == 1 ? reqData.gold_amount * await common_model.getCurrentVoryWightInGram() : reqData.gold_amount,
        "created_at": todayTime
    }

    let trangaction = await buy_sell_trangaction_model.addNewTempBuyAndSaleTransaction(temp_buy_sell_transaction);

    return res.send({
        "success": true,
        "message": "",
        "karat_type": reqData.karat_type,
        "cal_type": reqData.cal_type,
        "gold_amount": reqData.gold_amount,
        "gold_price": getGoldPriceAmount,
        "VAT": 0,
        "service_charge": service_charge.total_service_charge,
        "total": Math.round(getGoldPriceAmount - service_charge.total_service_charge),
        "track_no": track_no,
        "next_step": "sale-buy-sale-getaway",
        "service_charge_details": service_charge
    });
});

router.post('/confirm_gold_to_money_track_no', verifyToken, async (req, res) => {

    let reqData = {
        "track_no": req.body.track_no ?? "0"
    }

    let temp_buy_sell_info = await buy_sell_trangaction_model.getUserTempBuyAndSaleTransactionByTrackNo(reqData.track_no);

    if (isEmpty(temp_buy_sell_info)) {
        return res.send({
            "success": false,
            "message": "Unknown Buy Request",
            "next_step": "sale-buy-sale"
        });
    }

    if (temp_buy_sell_info[0].from_currency_type != "gold") {
        return res.send({
            "success": false,
            "message": "Wrong request",
            "next_step": "sale-buy-sale"
        });
    }

    if (temp_buy_sell_info[0].status == 0) {
        return res.send({
            "success": false,
            "message": "Already Complete",
            "next_step": "sale-buy-sale"
        });
    }


    let buy_sell_new = { ...temp_buy_sell_info[0] };
    if (buy_sell_new.hasOwnProperty('track_no')) delete buy_sell_new.track_no;
    if (buy_sell_new.hasOwnProperty('status')) delete buy_sell_new.status;
    if (buy_sell_new.hasOwnProperty('id')) delete buy_sell_new.id;

    let user_balance = await balance_model.getUserBalanceByUserId(temp_buy_sell_info[0].user_id);
    let karat_col_name = "gold_18_karat";

    let userOldBalance = 0
    if (isEmpty(user_balance)) {
        await balance_model.addNewBalance({ "user_id": temp_buy_sell_info[0].user_id });
    } else {
        if (temp_buy_sell_info[0].gold_type == 1) {
            userOldBalance = user_balance[0].gold_22_karat;
            karat_col_name = "gold_22_karat";
        } else if (temp_buy_sell_info[0].gold_type == 2) {
            userOldBalance = user_balance[0].gold_21_karat;
            karat_col_name = "gold_21_karat";
        } else if (temp_buy_sell_info[0].gold_type == 3) {
            userOldBalance = user_balance[0].gold_18_karat;
            karat_col_name = "gold_18_karat";
        } else {
            console.log("come");
        }
    }


    let userNewBalance = userOldBalance - temp_buy_sell_info[0].total_gold;
    let result = await buy_sell_trangaction_model.addNewBuy_sellAndUpdateTemp_buy_sellAndUserGoldBalance(temp_buy_sell_info[0].track_no, buy_sell_new, userNewBalance, karat_col_name, temp_buy_sell_info[0].user_id);

    return res.send({
        "success": true,
        "message": "SuccessFully Sale The Gold",
        "next_step": "move-to-balance-page"
    });
});

router.get('/', (req, res) => {
    return res.send({
        "success": true,
        "message": "",
        "api v": 1
    });
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;
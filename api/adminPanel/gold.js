const express = require("express");
const router = express.Router();

const isEmpty = require("is-empty");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var path = require('path');
const multer = require('multer');

const admin_model = require('../model/admin');
const user_model = require('../model/user');
const user_balance_model = require('../model/balance');
const gold_type_model = require('../model/gold_type');
const user_buy_sell_trangection_model = require('../model/buy_sell_trangaction');
const common_model = require('../model/common');
const verifyTokenWeb = require('../jwt/verify/verifyTokenWeb');

const url = "http://localhost:3001/admin_panel";

router.get(['/details/:karet'], async (req, res) => {

    let karate_id = parseInt(req.params.karet);

    if (karate_id == '22') karate_id = 1;
    else if (karate_id == '21') karate_id = 2;
    else if (karate_id == '18') karate_id = 3;
    else {
        return res.render('404', {
            message: "Unknown Page",
            page: "gold"
        });
    }

    let gold_info = await gold_type_model.getGoldTypeById(karate_id);

    if (isEmpty(gold_info)) {
        return res.render('404', {
            message: "Unknown Page",
            page: "gold"
        });
    }

    gold_info = gold_info[0];

    let gold_price_history = await gold_type_model.getGoldTypePriceHistoryListByGold_type_ID(karate_id);

    let year_month_chart = [];
    let vori_price_chart = [];
    let gram_price_chart = [];

    let max_loop = gold_price_history.length;
    for (i = max_loop - 1; i > -1; i--) {
        vori_price_chart.push(gold_price_history[i].vori_price);
        year_month_chart.push(await common_model.getFormatedDateForWebView(gold_price_history[i].incress_date));
        gram_price_chart.push(gold_price_history[i].gram_price);
    }

    year_month_chart = year_month_chart.join("-");

    let today = await common_model.getTodayDate();

    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return res.render('gold_details', {
        message: "",
        page: "gold",
        gold_price_history,
        vori_price_chart,
        year_month_chart,
        gram_price_chart,
        today,
        gold_info,
        months: months,
        success: true
    });
});


router.post('/createGoldPrice', async (req, res) => {

    // return res.send(req.body);

    let vori_price = parseInt(req.body.vori_price_form_create);
    let incress_date = req.body.incress_date_form_create;
    let karat = req.body.karat_form;


    let karate_id = 0;
    let gram_price = Math.round(vori_price / 11.664);

    if (karat == '22') karate_id = 1;
    else if (karat == '21') karate_id = 2;
    else if (karat == '18') karate_id = 3;
    else {
        return res.render('404', {
            message: "Unknown Request",
            page: "gold"
        });
    }

    // await gold_type_model.updateGoldTypePriceHistoryByID(id, vori_price, gram_price, incress_date);
    let gold_price_history = await gold_type_model.getGoldTypePriceHistoryListByGold_type_ID(karate_id);
    let todayTime = await common_model.getTodayDateTime();

    let newInserDate = {
        "dgb_gold_types_id": karate_id,
        "vori_price": vori_price,
        "gram_price": gram_price,
        "karat_title": karat,
        "created_at": todayTime,
        "incress_date": await common_model.getDateTimeFormateForDatabase(incress_date)
    };


    if (!isEmpty(gold_price_history)) {
        lastIncressDate = gold_price_history[0].incress_date;
        let needUpdate = await common_model.compareTwoDate(incress_date, lastIncressDate);

        if (needUpdate) {
            await gold_type_model.addNewGoldPriceTypeHistoryAndUpdateExistingGoldPriceType(newInserDate, karate_id, todayTime);
        } else {
            await gold_type_model.addNewGoldPriceTypeHistory(newInserDate);
        }

    } else {
        await gold_type_model.addNewGoldPriceTypeHistoryAndUpdateExistingGoldPriceType(newInserDate, karate_id, todayTime);
    }

    return res.redirect(url + "/gold/details/" + karat);
});


router.post('/updateGoldPrice', async (req, res) => {

    let vori_price = parseInt(req.body.vori_price_form);
    let gram_price = Math.round(vori_price / 11.664);
    let incress_date = req.body.incress_date_form;
    let id = req.body.id_form;
    let karat = req.body.karat_form;
    let karate_id = 0;

    if (karat == '22') karate_id = 1;
    else if (karat == '21') karate_id = 2;
    else if (karat == '18') karate_id = 3;
    else {
        return res.render('404', {
            message: "Unknown Request",
            page: "gold"
        });
    }

    await gold_type_model.updateGoldTypePriceHistoryByID(id, vori_price, gram_price, incress_date);
    let gold_price_history = await gold_type_model.getGoldTypePriceHistoryListByGold_type_ID(karate_id);

    if (!isEmpty(gold_price_history)) {
        if (gold_price_history[0].id == id) {
            let todayTime = await common_model.getTodayDateTime();
            await gold_type_model.updateGoldPriceTypeByID(karate_id, vori_price, gram_price, todayTime);
        }
    }

    return res.redirect(url + "/gold/details/" + karat);
});


router.get('/*', (req, res) => {
    return res.render('404', {
        message: "Page Not Found",
        page: "gold"
    });
});


router.post('/*', (req, res) => {
    return res.render('404', {
        message: "Page Not Found",
        page: "gold"
    });
});



module.exports = router;
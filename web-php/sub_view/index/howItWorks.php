<section id="goldFunctioning">
    <div class="pos-rel container web-align">
        <div class="col l2 gf93MainHeading">How It Works</div>
        <div class="col l2 gf93SubHeading para">Get started with these easy steps.</div>
        <div class="pos-abs phiw93TabsContainer" style="border-bottom:none!important;">
            <div class="tabs8Container">

                <ul class="nav nav-tabs" role="tablist" style="width:150px;border:none!important;margin-left: -50px;">
                    <li role="presentation" class="active"><a class='tabb' href="#nav-buy-home" aria-controls="home" role="tab" data-toggle="tab">BUY</a></li>
                    <li role="presentation"><a href="#nav-sale-home" class='tabb' aria-controls="profile" role="tab" data-toggle="tab">Sale</a></li>
                </ul>

            </div>
        </div>



        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="nav-buy-home">
                <div class="modal-body" id="div_modal_buy_id">

                    <div class="row phiw93Wrapper">
                        <div class="col-sm-4">
                            <div>
                                <div class="phiw93StepNumber">1</div>
                                <div class="phiw93Heading">Enter the amount</div>
                                <div class="phiw93Line"></div>
                                <div class="phiw93Para">You can input your desired purchase amount in rupees or grams of gold.</div>
                            </div>
                        </div>
                        <div class="col-sm-4 col l4 phiw93StepsDiv">
                            <div class="phiw93StepArrow"><i class="material-icons is31Default primaryClr fs18" style="width: 18px; height: 18px;">arrow_forward</i></div>
                            <div>
                                <div class="phiw93StepNumber">2</div>
                                <div class="phiw93Heading">Buy in one-click</div>
                                <div class="phiw93Line"></div>
                                <div class="phiw93Para">Review your order and place order using UPI/Netbanking within the 5-minute price window.</div>
                            </div>
                        </div>
                        <div class="col-sm-4 col l4 phiw93StepsDiv">
                            <div class="phiw93StepArrow"><i class="material-icons is31Default primaryClr fs18" style="width: 18px; height: 18px;">arrow_forward</i></div>
                            <div>
                                <div class="phiw93StepNumber">3</div>
                                <div class="phiw93Heading">Secured and insured</div>
                                <div class="phiw93Line"></div>
                                <div class="phiw93Para">Your gold locker will be updated, where you can view your purchased Digital Gold holdings.</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div role="tabpanel" class="tab-pane" id="nav-sale-home">
                <div class="modal-body" id="div_modal_sale_id">

                    <div class="row phiw93Wrapper">
                        <div class="col-sm-4">
                            <div>
                                <div class="phiw93StepNumber">1</div>
                                <div class="phiw93Heading">Enter units to sell</div>
                                <div class="phiw93Line"></div>
                                <div class="phiw93Para">The current selling price will be shown.</div>
                            </div>
                        </div>
                        <div class="col-sm-4 col l4 phiw93StepsDiv">
                            <div class="phiw93StepArrow"><i class="material-icons is31Default primaryClr fs18" style="width: 18px; height: 18px;">arrow_forward</i></div>
                            <div>
                                <div class="phiw93StepNumber">2</div>
                                <div class="phiw93Heading">Sell in one-click</div>
                                <div class="phiw93Line"></div>
                                <div class="phiw93Para">Review and place your sell request within the 5-minute price window.</div>
                            </div>
                        </div>
                        <div class="col-sm-4 col l4 phiw93StepsDiv">
                            <div class="phiw93StepArrow"><i class="material-icons is31Default primaryClr fs18" style="width: 18px; height: 18px;">arrow_forward</i></div>
                            <div>
                                <div class="phiw93StepNumber">3</div>
                                <div class="phiw93Heading">Request Confirmed</div>
                                <div class="phiw93Line"></div>
                                <div class="phiw93Para">Your gold locker will be updated, where you can view your Digital Gold balance.</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</section>
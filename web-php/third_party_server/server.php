<?php
// session_start();

class Server
{
  // Properties
  public $host = "http://localhost:3001/api/v1";
  public $request;
  public $date;

  function __construct()
  {
    // $this->$host = $host;
  }

  //   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  //     'Header-Key: Header-Value',
  //     'Header-Key-2: Header-Value-2'
  // ));

  // Methods
  function get_date($req_url, $request_data, $sent_token)
  {
    // $dataArray = array("s" => 'PHP CURL');
    $data = http_build_query($request_data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $this->host . $req_url . "?" . $data);

    if ($sent_token == true) {
      $token = "";

      if (isset($_SESSION['dg_bangladesh_token'])) {
        $token =  $_SESSION['dg_bangladesh_token'];
      }

      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'x-access-token: ' . $token
      ));
    }

    $server_output = curl_exec($ch);
    curl_close($ch);

    if ($server_output == "OK") {
      echo "come fail 999";
      return 0;
    } else {
      $result = json_decode($server_output, true);
      return $result;
    }
  }

  function get_with_date($url, $date)
  {
    return 0;
  }


  function post_req($req_url, $data, $sent_token)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $this->host . $req_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

    if ($sent_token == true) {

      $token = "";

      if (isset($_SESSION['dg_bangladesh_token'])) {
        $token =  $_SESSION['dg_bangladesh_token'];
      }

      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'x-access-token: ' . $token
      ));
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close($ch);

    if ($server_output == "OK") {
      echo "come fail 999";
      return 0;
    } else {
      $result = json_decode($server_output, true);
      return $result;
    }
    return 0;
  }
}

// $server_obj = new Server();
// $outpur = $server_obj->post_req("/auth/log_reg");
// // print_r($outpur);

// $server_obj->get_without_date("/get");

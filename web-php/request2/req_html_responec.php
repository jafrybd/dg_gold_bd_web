<?php
class HTML_Responce
{

    public $responce_html = array(
        "buy-sell-buy" =>  '<div class="form-group">' .
            '<p>Money</p>' .
            '<input type="number" oninput="updateGoldCalculation(\'money\', \'1\')" class="form-control" id="form_money_buy" name="form_money_buy" required>' .
            '</div>' .

            '<div class="form-group">' .
            ' <p>Gold</p>' .
            ' <div class="row">' .
            '<div class="col-sm-8">' .
            '<input type="number" oninput="updateGoldCalculation(\'gold\', \'1\')" class="form-control" id="form_gold_buy" name="form_gold_buy" required>' .
            '</div>' .
            '<div class="col-sm-4">' .
            '<select name="gold_cal_type_buy" class="form-control" onchange="updateGoldCalculation(\'money\', \'1\')" id="gold_cal_type_buy">' .
            '<option value="1">Vori</option>' .
            ' <option value="2">gm</option>' .
            '</select>' .
            '</div>' .
            '</div>' .
            '</div>' .
            '<button type="button" onclick="check_buy()" class="btn btn-primary">Continue</button>',

        "buy-sell-buy-getaway" => '<div align="center">' .
            '<img src="./images/bkash.jpg" alt="Bkash" height="330px" width="350px">' .
            '</div>' .
            '<br>' .
            '<button type="button" onclick="confirm_buy()" class="btn btn-primary">Confirm</button>',

        "sale-buy-sale" =>  '<div class="form-group">' .
            '<p>Gold</p>' .
            '<div class="row">' .
            '<div class="col-sm-8">' .
            '<input type="number" oninput="updateGoldCalculation(\'gold\', \'2\')" class="form-control" id="form_gold_sale" name="form_gold_sale" required>' .
            ' </div>' .
            '<div class="col-sm-4">' .
            '<select name="gold_cal_type_sale" oninput="updateGoldCalculation(\'money\', \'2\')" class="form-control" id="gold_cal_type_sale">' .
            '<option value="1">Vori</option>' .
            '<option value="2">gm</option>' .
            '</select>' .
            '</div>' .
            '</div>' .
            '</div>' .
            ' <div class="form-group">' .
            '<p>Money</p>' .
            '<input type="number" oninput="updateGoldCalculation(\'money\', \'2\')" class="form-control" id="form_money_sale" name="form_money_sale" required>' .
            '</div>' .

            '<button type="button" onclick="check_sale()" class="btn btn-primary">Continue</button>',

        "sale-buy-sale-getaway" =>  '<div align="center">' .
            '<img src="./images/bkash.jpg" alt="Bkash" height="330px" width="350px">' .
            '</div>' .
            '<br>' .
            '<button type="button" onclick="confirm_sale()" class="btn btn-primary">Confirm</button>',
    );

    function get_html_by_next_step_name($step_name, $message, $track_no_buy, $track_no_sale, $otherInfo)
    {
        // echo "message : " .$message;


        if ($step_name == 'buy-sell-buy-2') {
            $res_html = '<div class="form-group" id="">' .
                '<br> <p> Gold Amount: ' . $otherInfo['gold_amount'] . ' ' . $otherInfo['cal_type']  . '</p>' .
                '<p> Gold Type: ' . $otherInfo['karat_type'] . ' Karate</p>' .
                '<p> Gold Price: ' . $otherInfo['gold_price'] . '</p>' .
                '<p> VAT: ' . $otherInfo['VAT'] . '</p>' .
                '<p> Total Amount: ' . $otherInfo['total'] . '</p>' .
                '<input type="hidden" class="form-control" id="form_gold_buy" name="form_gold_buy" value="' . $otherInfo['gold_amount'] . '" required>' .
                '<input type="hidden" class="form-control" id="gold_cal_type_buy" name="gold_cal_type_buy" value="' . $otherInfo['cal_type_main'] . '" required>' .
                '<input type="hidden" class="form-control" id="form_karat_type_buy" name="form_karat_type_buy" value="' . $otherInfo['karat_type'] . '" required>' .
                '<button type="button" onclick="check_buy2()" class="btn btn-primary">Confirm</button>' .
                '<button type="button" onclick="back_buy()" class="btn btn-primary">Back</button>' .
                '</div>';
        } else if ($step_name == 'sell-buy-sell2') {
            $res_html = '<div class="form-group" id="">' .
                '<br> <p> Gold Amount: ' . $otherInfo['gold_amount'] . ' ' . $otherInfo['cal_type']  . '</p>' .
                '<p> Gold Type: ' . $otherInfo['karat_type'] . ' Karate</p>' .
                '<p> Gold Price: ' . $otherInfo['gold_price'] . '</p>' .
                '<p> Service Charge: ' . $otherInfo['service_charge'] . '</p>' .
                '<p> VAT: ' . $otherInfo['VAT'] . '</p>' .
                '<p> Total Amount: ' . $otherInfo['total'] . '</p>' .
                '<input type="hidden" class="form-control" id="form_gold_sale" name="form_gold_sale" value="' . $otherInfo['gold_amount'] . '" required>' .
                '<input type="hidden" class="form-control" id="gold_cal_type_sale" name="gold_cal_type_sale" value="' . $otherInfo['cal_type_main'] . '" required>' .
                '<input type="hidden" class="form-control" id="form_karat_type_sale" name="form_karat_type_sale" value="' . $otherInfo['karat_type'] . '" required>' .
                '<button type="button" onclick="check_sale2()" class="btn btn-primary">Confirm</button>' .
                '<button type="button" onclick="back_sale()" class="btn btn-primary">Back </button>' .
                '</div>';
        } else {
            $res_html =  $this->responce_html[$step_name];
        }


        if ($message != "") {
            $res_html = $res_html . '<p>' . $message . '</p>';
        }


        if ($track_no_buy != 0 && $track_no_buy != "") {
            $res_html = $res_html . '<input type="hidden" class="form-control" id="form_track_no_buy" name="form_track_no" value="' . $track_no_buy . '" required>';
        }

        if ($track_no_sale != 0 && $track_no_sale != "") {
            $res_html = $res_html . '<input type="hidden" class="form-control" id="form_track_no_sale" name="form_track_no" value="' . $track_no_sale . '" required>';
        }

        return  $res_html;
    }
}

<?php

session_start();
if (isset($_SESSION['dg_bangladesh_user_name'])) {
    unset($_SESSION['dg_bangladesh_user_name']);
}

if (isset($_SESSION['dg_bangladesh_token'])) {
    unset($_SESSION['dg_bangladesh_token']);
}

if (isset($_SESSION['dg_bangladesh_user_email'])) {
    unset($_SESSION['dg_bangladesh_user_email']);
}

if (isset($_SESSION['dg_bangladesh_track_email'])) {
    unset($_SESSION['dg_bangladesh_track_email']);
}

if (isset($_SESSION['dg_bangladesh_track_no'])) {
    unset($_SESSION['dg_bangladesh_track_no']);
}

header('Location: index.php');

?>
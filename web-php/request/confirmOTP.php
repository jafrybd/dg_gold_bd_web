<?php

include_once("req_html_responec.php");
include_once("../third_party_server/server.php");

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $OTP = "dsa.com";
    $track_no = "dsa.com";

    if (isset($_POST['form_OTP'])) {
        $OTP = $_POST['form_OTP'];
    }

    if (isset($_POST['form_track_no'])) {
        $track_no = $_POST['form_track_no'];
    }

    $req_data = [
        "otp" => $OTP,
        "track_no" => $track_no
    ];

    $server_obj = new Server();
    $responce = $server_obj->post_req("/auth/otp_check", $req_data, false);
    $next_step = "";
    $message = "";
    $email = "";

    // print_r($responce);

    if ($responce != 0) {
        if ($responce['success']  == 1) {
            $next_step = $responce['next_step'];
            if ($next_step == 'home-page') { // login
                $user_name = $responce['user_info']['user_name'];
                $user_email = $responce['user_info']['user_email'];
                $token = $responce['token'];

                $_SESSION['dg_bangladesh_user_name'] =  $user_name;
                $_SESSION['dg_bangladesh_token'] = $token;
                $_SESSION['dg_bangladesh_user_email'] = $user_email;

                echo 0;
                return 0;
            } else { //reg
                $message = $responce['message'];
                $_SESSION['dg_bangladesh_track_no'] =  $track_no;
            }
        } else {
            $next_step = $responce['next_step'];
            if ($next_step == 're-check-OTP-page') {
                $next_step = 'check-OTP';
                $message = $responce['message'];
            } else {
                $next_step = 'login-page';
                $message = "";
            }
        }
    } else {
        $next_step = 'login-page';
        $message = 'Try again';
    }


    if ($next_step != "") {

        $html_responce_object = new HTML_Responce();
        $html_responce = $html_responce_object->get_html_by_next_step_name($next_step, $message, $track_no);
        echo $html_responce;
    }
}

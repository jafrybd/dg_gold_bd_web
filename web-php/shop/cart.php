<?php

session_start();
if (!isset($_SESSION['dg_bangladesh_token'])) {
    header("location: ../index.php");
} else {

    include_once("../third_party_server/server.php");
    $server_obj = new Server();
    $reqData = array();
    $response = $server_obj->get_date("/jewellery-cart/list", $reqData, true);

    $product_list = array();
    $current_balance = 0;
    $making_charge_percent = 0;
    $making_charge = 0;
    $total_gold_gm = 0;
    $delivery_charge = 500;

    // print_r($response);
    if ($response['success'] == 1) {
        $product_list = $response['product_list'];
        $current_balance = $response['current_balance'];
        $making_charge_percent = $response['making_charge_percent'];
        $making_charge = $response['making_charge'];
        $total_gold_gm = $response['total_gold_gm'];
        $delivery_charge = $response['delivery_charge'];
    }
?>


    <!DOCTYPE html>
    <html lang="en">
    <?php include_once('sub_view/head.php'); ?>

    <body>

        <?php include_once('sub_view/header.php'); ?>
        <!-- / header -->

        <?php include_once('sub_view/nav.php'); ?>
        <!-- / navigation -->

        <div id="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li>Cart</li>
                </ul>
            </div>
            <!-- / container -->
        </div>

        <div id="body">
            <div class="container">
                <?php if (count($product_list) < 1) {
                    $delivery_charge = 0 ?>
                    <div style="text-align: center">
                        <img class="center" src="images/empty_cart.png" alt="product image">
                    </div>
                <?php } else { ?>
                    <div id="content" class="full">
                        <div class="cart-table">
                            <table>
                                <tr>
                                    <th class="items">Items</th>
                                    <th class="price">Weight</th>
                                    <th class="qnt">Quantity</th>
                                    <th class="total">Total Weight</th>
                                    <th class="delete"></th>
                                </tr>
                                <?php for ($i = 0; $i < count($product_list); $i++) { ?>
                                    <tr>
                                        <td class="items">
                                            <div class="image">
                                                <img src="images/<?php echo  $product_list[$i]['product_details']['image']; ?>" alt="product image">
                                            </div>
                                            <h3><a href="product-detals.php?id=<?php echo $product_list[$i]['id']; ?>"><?php echo  $product_list[$i]['product_details']['title']; ?></a></h3>
                                            <p><?php echo  $product_list[$i]['product_details']['description']; ?></p>
                                        </td>
                                        <td class="price" style="text-align: center;">
                                            <?php echo  $product_list[$i]['product_details']['karate_weight_gm'] . " gm"; ?>
                                            <p> or </p>
                                            <?php echo  $product_list[$i]['product_details']['karate_weight_gm'] / 11.664 . " vori"; ?>
                                        </td>
                                        <td class="qnt"><select>
                                                <option><?php echo  $product_list[$i]['quantity']; ?></option>
                                            </select></td>
                                        <td class="total" style="text-align: center;">
                                            <?php echo  $product_list[$i]['product_details']['karate_weight_gm'] * $product_list[$i]['quantity'] . " gm"; ?>
                                            <p> or </p>
                                            <?php echo ($product_list[$i]['product_details']['karate_weight_gm'] * $product_list[$i]['quantity']) / 11.664 . " vori"; ?>

                                        </td>
                                        <td class="delete"><a href="#" onclick="deleteFromCart('<?php echo $product_list[$i]['id']; ?>')" class="ico-del"></a></td>
                                    </tr>
                                <?php  } ?>
                            </table>
                        </div>

                        <div class="total-count">
                            <h4>Your Current Balance: <strong> <?php echo  $current_balance . " gm"; ?> </strong></h4>
                            <h4>Total Order Gold: <strong> <?php echo  $total_gold_gm . " gm"; ?> </strong></h4>
                            <p>Making_charge: <?php echo  $making_charge . " Tk"; ?> ( <?php echo  $making_charge_percent; ?>% of Total Gold Price)</p>
                            <p>Delivery Cost: <?php echo  $delivery_charge . " Tk"; ?> </p>
                            <h3>Total to pay: <strong><?php echo  $making_charge + $delivery_charge; ?> Tk</strong></h3>

                            <?php if ($current_balance >= $total_gold_gm) { ?>
                                <a href="#" onclick="orderConfirm()" class="btn-grey">Finalize and Confirm</a>
                            <?php  } ?>
                        </div>

                    </div>
                <?php } ?>


                <!-- / content -->
            </div>
            <!-- / container -->
        </div>
        <!-- / body -->


        <?php include_once('sub_view/footer.php'); ?>
        <!-- / footer -->

        <?php include_once('sub_view/script.php'); ?>

        <!-- // Ajax  -->

        <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

        <script>
            function deleteFromCart(id) {
                $.ajax({
                    url: '../shop_request/remove_from_cart.php',
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        cart_id: id
                    },
                    success: function(response) {
                        if (response == 1990) {
                            window.location = '../logout.php'
                        } else {
                            window.location = 'cart.php'
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        console.log("Fail");
                    }

                });

            }
        </script>


        <script>
            function orderConfirm() {
                $.ajax({
                    url: '../shop_request/order_confirm.php',
                    type: 'POST',
                    dataType: 'html',
                    success: function(response) {
                        if (response == 1990) {
                            window.location = '../logout.php'
                        } else {
                            window.location = 'cart.php'
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        console.log("Fail");
                    }

                });

            }
        </script>


    </body>

    </html>

<?php } ?>
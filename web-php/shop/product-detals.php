<?php

session_start();
if (!isset($_SESSION['dg_bangladesh_token'])) {
	header("location: ../index.php");
} else {
	$product_id = isset($_GET['id']) ? $_GET['id'] : 0;

	include_once("../third_party_server/server.php");
	$server_obj = new Server();
	$reqData = array("product_id" => $product_id);
	$response = $server_obj->get_date("/product/details", $reqData, true);
	$product_details = array();

	if ($response['success'] == 1) {
		$product_details = $response['details'];
	}
?>

	<!DOCTYPE html>
	<html lang="en">
	<?php include_once('sub_view/head.php'); ?>

	<body>

		<?php include_once('sub_view/header.php'); ?>
		<!-- / header -->

		<?php include_once('sub_view/nav.php'); ?>
		<!-- / navigation -->


		<!-- / body -->

		<div id="body">
			<div class="container">
				<div id="content" class="full">
					<?php if (empty($product_details)) { ?>
						<h2> Unknown Request </h2>
					<?php } else { ?>
						<div class="product">
							<div class="image">
								<img src="images/<?php echo  $product_details['image']; ?>" alt="product Image">
							</div>
							<div class="details">
								<h1> <?php echo $product_details['title']; ?></h1>
								<h4> <?php echo $product_details['karate_weight_gm']; ?> gm</h4>
								<div class="entry">
									<p><?php echo $product_details['mini_description']; ?></p>
									<div class="tabs">
										<div class="nav">
											<ul>
												<li class="active"><a href="#desc">Description</a></li>
												<li><a href="#spec">Delivery</a></li>
												<li><a href="#ret">Returns</a></li>
											</ul>
										</div>
										<div class="tab-content active" id="desc">
											<p> <?php echo $product_details['description']; ?></p>
										</div>
										<div class="tab-content" id="spec">
											<p><?php echo $product_details['delivery']; ?></p>
										</div>
										<div class="tab-content" id="ret">
											<p><?php echo $product_details['returns']; ?></p>
										</div>
									</div>
								</div>
								<div class="actions">
									<!-- <label>Quantity:</label>
									<select>
										<option>1</option>
									</select> -->
									<a href="#" onclick="addToCart('<?php echo $product_details['id']; ?>')" class="btn-grey">Add to cart</a>
								</div>
							</div>
						</div>
					<?php } ?>

				</div>
				<!-- / content -->
			</div>
			<!-- / container -->
		</div>
		<!-- / body -->


		<?php include_once('sub_view/footer.php'); ?>
		<!-- / footer -->

		<?php include_once('sub_view/script.php'); ?>

		<script>
			function addToCart(id) {
				$.ajax({
					url: '../shop_request/add_to_cart.php',
					type: 'POST',
					dataType: 'html',
					data: {
						product_id: id
					},
					success: function(response) {
						if (response == 1990) {
							window.location = '../logout.php'
						} else {
							// alert("Product Added to Cart.")
							window.location = 'cart.php'
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log("Fail");
					}

				});

			}
		</script>

	</body>

	</html>

<?php } ?>